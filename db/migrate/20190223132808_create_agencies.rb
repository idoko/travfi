class CreateAgencies < ActiveRecord::Migration[5.2]
  def change
    create_table :agencies do |t|
      t.string :trade_name
      t.string :address
      t.string :state
      t.string :city
      t.string :postal_code
      t.string :telephone
      t.string :website
      t.string :booking_number
      t.string :company_number
      t.string :iata_number
      t.timestamps
    end
  end
end
