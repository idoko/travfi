require 'bcrypt'
class HomeController < ApplicationController
    def index
    end

    def login
    end

    def do_login
        email = params[:email]
        password = params[:password]
        user = User.find_by_email(email)
        if user && user.password === password
            session[:user_id] = user.id
            redirect_to '/apply'
        else
            flash.now[:error] = "Incorrect username and/or password"
            redirect_to '/login'
        end
    end

    def do_signup
        @user = User.new(signup_params)
        @user.password = BCrypt::Password.create(params[:password])
        if @user.save
            session[:user_id] = @user.id
            redirect_to ('/apply')
        else
            flash.now[:error] = "Could not sign you up at the moment. Please try later"
            redirect_to ('/login#toregister')
        end
    end

    def apply
    end

    def do_apply
        @agency = Agency.new(register_params)
        if @agency.save
            flash.now[:success] = "You have successfully registered. We can't wait to work with you"
        else
            flash.now[:error] = "We ran into some trouble. Please try later"
        end
        return redirect_to '/apply'
    end

    def logout
        session[:user_id] = nil
        redirect_to '/login'
    end
    private
        def login_params
            params.require(:user).permit(:email, :password)
        end

        def signup_params
            params.require(:user).permit(:username, :email, :password)
        end

        def register_params
            params.require(:agency).permit!
        end
end
