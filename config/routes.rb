Rails.application.routes.draw do
  root 'home#index'
  get '/login' => 'home#login'
  get '/signup', to: redirect('/login#toregister')
  post '/login' => 'home#do_login'
  post '/signup' => 'home#do_signup'

  get '/apply' => 'home#apply'
  post '/apply' => 'home#do_apply'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
